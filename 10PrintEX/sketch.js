//I have changed the rules to:
// 1) draw a backsplash and forward slash every time the random picks a number
// between 1 and 20 that is below 5.
// 2) else only draw a forward slash

let x = 0;
let y = 0;
let spacing = 10;

function setup() {
  createCanvas(windowWidth, windowHeight);
  background(0);
}

function draw() {
  // stroke(255);
  if (random(20) < 5) {
    stroke(255,0,0);
    line(x, y+spacing, x+spacing, y);
    line(x, y, x+spacing, y+spacing);
  } else {
      stroke(0,0,255);
    line(x, y, x+spacing, y+spacing);
  }
  x+=10;
  if (x > width) {
    x = 0;
    y += spacing;
  }
}

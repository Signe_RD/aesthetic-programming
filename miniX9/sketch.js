
let url = "https://api.whatdoestrumpthink.com/api/v1/quotes";
let getQuote;
let trump;
let twitter;
let button;

function preload(){
	trump=loadImage('tronalddump.png');
	twitter=loadImage('twitter.png');
}

function setup() {
	createCanvas(windowWidth,windowHeight);
	background(255);
	fetchQuote();
	follow();
}

function fetchQuote() {
	request = url
	console.log("request", request);
	loadJSON(request, gotData); //this is the key syntax to make API request
}


function gotData(data) {
	clear(); // to clear the canvas before a new quote

		//Get quote
		i = floor(random(0, 47));
		getQuote = data.messages.non_personalized[i];
		console.log("getQuote", getQuote);

		//pictures
	push();
		translate(30, 0);
		image(trump,8,65,80,80);//Tronald Dump picture https://www.tronalddump.io/
		image(twitter,360,72,70,40); //Twitter verified
	pop();

		//profile
	push();
		translate(50, 0);
		textSize(35);
		fill(0);
		textStyle(BOLD);
		text('Tronald J. Dump', 80, 100); //name inspiration https://www.tronalddump.io/
		textSize(20);
		textStyle(NORMAL);
		fill(100);
		text('@realDonaldTrump', 80, 140); //Donald Trumps real twitter tag
	pop();

		//lines
	push();
		translate(0, 120);
		line(40, 200, 650, 200);
		line(40, 270, 650, 270);

		line(250, 200, 250, 270);
		line(425, 200, 425, 270);
	pop();

//quote
push();
	fill(0);
	textSize(28);
	text(getQuote, 38, 180, 600, 180);
pop();

	//likes&retweets
	push();
		translate(0, 120);
		textSize(20);
		fill(100);
		let rcount=floor(random(0,2000000));//retweet counter
		let lcount=floor(random(0,2000000));//like counter
		let ccount=floor(random(0,2000000));//comments counter
		text("retweets: ",58,220);
		text("likes: ",273,220);
		text("comments: ",450,220);
		fill(0,20,200);
		textStyle(BOLD);
		text(rcount, 58, 250);
		text(lcount, 273, 250);
		text(ccount, 450, 250);
	pop();
}


function follow() {
	//refresh button
	button = createButton('Follow');
	button.position(580, 80);
	button.mousePressed(fetchQuote);
}

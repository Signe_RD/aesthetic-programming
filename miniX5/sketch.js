let x = 0;
let y= 0;
// let y = 0; I wanted to create a grid first, but I chose not to, because it got to controlled
let spacing = 50; //the distant between the ellipses

function setup() {
  createCanvas(800,600);
  background(76, 123, 129);
 }

function draw() {
  frameRate(0.5); //I wanted to slow down the program to only 0.5 frame pr. second
for(let x=0;x<width;x+=spacing){ //the for loop
    if(random(2)<1.5){ //conditional statement that control when black ellipses is drawn
      fill(0);
      ellipse(x,random(0,600),20,20)
      if (random(7)>1){
      console.log('Lars'); //conditional statement that control when Lars is written in the console.log
      }
  }
  if(random(10)<0.2){ //conditional statement that control when white ellipses is drawn
    noStroke();
    fill(255,200);
    ellipse(x,random(0,600),20,20);
    if (random(7)<1){ //conditional statement that control when Woman is written in the console.log
    console.log('Woman');
    }
  }
}
}

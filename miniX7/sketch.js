//this code is made from the Daniel Shiffmans codeing challenge about the snake game
//I can't not take credit for most of the code, but I have rewritten it and I do understand it

let p;
let scl = 30;
let friends;
let mode=0; //starting mode

function setup() {
  createCanvas(600,600);

  p = new People(); //create new people
  frameRate(8);
  pickLocation();
}

function pickLocation(){ //pick the location where the friends is going to be placed
  let cols = floor(width/scl);
  let rows = floor(height/scl);

  friends = createVector(floor(random(cols)),floor(random(rows)));
  friends.mult(scl);
}


function draw() {
  background(77,152,87);

  if (mode==0){ //starting mode --> intro text
    let lines = ('Do you want a beer?\n Get as many friends along as possible\nPress space to start')
    fill(0);
    textSize(25);
    textAlign(CENTER);
    textLeading(30); //space between the lines
    text(lines,295,250)

  } else if(mode==1){ //game mode
    p.death();
    p.update();
    p.show();

    if (p.pick(friends)){ //pick up the friends
      pickLocation();
    }

    //create friends
    noStroke();
    fill(155);
    ellipse(friends.x,friends.y+(scl/2),scl,scl);

    displayScore(); //show how many friends you have picked up
  }
}

function displayScore() { //function to display how many friends you have picked up
    fill(50, 160);
    textSize(20);
    text('You have picked up '+ p.total + " friend(s)", 150, height-20);
}


function keyPressed() { //function to decide the direction of the snake through pressing different keys
  if (keyCode === 32){ //to change mode
    if(mode==0){
      mode++;
    } else if(mode==1){
      mode=1;
    }
  }

  if (keyCode === UP_ARROW) {
    p.dir(0, -1);
  } else if (keyCode === DOWN_ARROW) {
    p.dir(0, 1);
  } else if (keyCode === RIGHT_ARROW) {
    p.dir(1, 0);
  } else if (keyCode === LEFT_ARROW) {
    p.dir(-1, 0);
  }
}

### **ReadMe MiniX6** ###

[See my project here!](https://signe_rd.gitlab.io/aesthetic-programming/miniX6/)

[See my code here!](https://gitlab.com/Signe_RD/aesthetic-programming/-/blob/master/miniX6/sketch.js)

![](emoji1.png)
![](emoji2.png)

*note: Gitlab would not let me upload my font, so I changed it to the defualt font, but that is not the idea - so the pictures shows how the font should look like. 


**Which miniX and what is changed**

I have chosen to work with [miniX2](https://gitlab.com/Signe_RD/aesthetic-programming/-/tree/master/miniX2). I have chosen this because it is one of the miniXs that I got feedback on, it is also one of my miniXs with the most in depth conceptual idea, but I wasn’t happy with the outcome and the technical part. 

A short recap of my miniX2: I created two emojies, one static and one dynamic. The static emoji was meant to symbolize the standard emoji that we all know, and the dynamic emoji that changes size, facial features and color symbolize how no human is the same, so why should our emojies be. I never thought about my emojies as something that could or should be implemented, but more as something that hopefully could start a conversation about the problems with standard emojies. 

I have reorganized the code to make it more readable. I have made two more functions: dynamicEmoji and staticEmoji. I have fixed the problematics there were with the text with the dynamic emoji, by using push and pop. I think the strokes in the drawing of the dynamic emoji was affecting the text, and by placing it in push and pop it now only affects the drawing. I have changed the colors for the dynamic emoji. Originally, I wanted to work with different skincolors in my miniX2, and I tried to get it to work for a long time, but I couldn’t.  But after getting feedback on my miniX2 and working and thinking about my program, I have decided to go with completely random colors, to really focus on the problematics on emojies not being size inclusive and not being representative for all faces (with different facial features). You could argue that the colors in my first version of this program (miniX2) were random, but they were made with an idea of creating different skincolors, and the colors were picked randomly from the arrays that were made to create different skincolors, so it wasn’t really random. I still want to point out the problematics with emojies and different skincolors, and how adding skin modifiers  isn't a fix, but maybe the opposite, and I try to put focus on this problem by having nontraditional human skin colors for my dynamic emoji. 

**Aesthetic programming in my work?**

I have tried to demonstrate aesthetic programming in my work through the critique of standard emojies and their lack of diversity. This relates closely with what Soon & Cox writes: _"Aesthetic programming in this sense is considered as a practice to build things, and make worlds, but also produce immanent critique drawing upon computer science, art, and cultural theory."_ (Soon & Cox, p.15). I have tried to create something that could start a debate, rather than trying to come up with an answer. 

I hope that my program/work can be seen as a start push to create a debate and as a critique. I really like this quote _"(...)programming becomes a kind of “force-field"_ (Soon & Cox, p.15), and I find the point really important. I see programming as a way to push some boundaries and start conversations, and therefore I really like the idea of seeing it as a _"force-field"_. 


**What does it mean by programming as a practice, or even as a method for design?**

I think that this quote from Soon & Cox describes how programming for me has become a tool: _"It is offered as a deep learning “tool” in its widest sense — a handbook for those unaccustomed to programming — that allows for the programmer’s conceptual skills to develop as they become technically more proficient."_ (Soon & Cox, p.13). They are referring to the book and the idea of aesthetic programming in this quote, but for me, I wouldn’thave had any idea where to start if I were to learn how to program without this book as a guideline. It has helped me understand how programming can be used to ask difficult question and raise awareness, which some of things that I really want to do with my coding. I see programming and writing code for a tool to design and investigate different topics. I see programming as new (new for me) a way to ask questions and investigate different problematics in the world. This also relates well we the following quote, but because  of the limited characters, I will not talk more about it, I just want to include it: _"We believe that paying attention to fundamental, or key, concepts from programming provides the opportunity to open up new insights into aesthetics and critical theory, as well as new perspectives on cultural phenomena increasingly bound to computational logic."_ (Soon & Cox, p.16)

**Programming and digital culture?**

I see programming and code as a big part of most people's everyday life in 2021. Most of us use some form of technical devices in every day, and these devices consist of some sort of hardware, but more importantly (more important in relations to the question) some programed software. Our digital culture has only expanded with time and we use programmed things, apps, programs, webpages etc. every single day and I therefore think that is important that we also understand how this is made and how some (read most) programmed things isn't really that neutral. So, I think we need to understand programming and by who and how it is written to really understand how it affect our digital life and our digital culture. 

**Links between the Aesthetic Programming/Critical Making texts/concepts and my work - critical-aesthetics**

To answer this question,  I want to start of with one of my favourite quotes from Ratto & Hertz: _"critical making is about turning the relationship between technology and society from a 'matter of fact' into a matter of concern'"_ (Matt Ratto & Garnet Hertz, p.20). I think this quote sums up the idea of critical making, and how we can use it to ask hard questions and create a debatw about something. The idea of changing the relationship and the debatw about technology and society from something facts based to something that ask questions and that are concerned with the problematics, is so important. I try to create programs that asks more questions instead of giving an answer, beacuse I think most of the time there isn't one answer there are 1000, but at the same time there is 1.000.000 more questions/problematics. 

I want to include one final concepts from the notion of critical making to my ReadMe. Ratto and Hertz writes _"the process of being critical starts by denaturalizing standard assumptions, values, and norms in order to reflect on the position and role of specific technologies within society"_ (Matt Ratto & Garnet Hertz, p.22), and I think this relates to my program very well. I have created a very non-standard emoji (the dynamic one), to create a debate about the standard emojies and how the fact that many people can't relate to them and how we need to find a new standard and how there probably isn't one neutral starting point any more, and how there probably never was one. 

**References:**
- Soon Winnie & Cox, Geoff, "Preface", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 12-24
- Ratto, Matt & Hertz, Garnet, “Critical Making and Interdisciplinary Learning: Making as a Bridge between Art, Science, Engineering and Social Interventions” In Bogers, Loes, and Letizia Chiappini, eds. The Critical Makers Reader: (Un)Learning Technology. the Institute of Network Cultures, Amsterdam, 2019, pp. 17-28 

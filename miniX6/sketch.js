let circleW = [200, 150, 220, 120, 190];
let circleH = [200, 160, 250, 170, 230];
let eye1 = 310;
let eye2 = 390;
let eyey = 245;
let eyeW = [35, 25, 30];


let smileW = [50, 80, 60];
let noseW =[10,20,30]

// Gitlab would not let me upload my font 
// let myFont;
// function preload() {
// myFont = loadFont('font1.otf');
// }

function setup() {
createCanvas(700, 500);
frameRate(5);
}

function draw() {
background(200);
if (keyCode==32){
dynamicEmoji();
} else {
staticEmoji()
}
if (keyCode==13){
staticEmoji();
}
}

function dynamicEmoji(){
push();
  //face
  noStroke();
  fill(random(0,255),random(0,255),random(0,255));
  frameRate(3);
  ellipse(350, 270, random(circleW), random(circleH));
  //eyes
  noStroke();
  fill(255)
  ellipse(eye1, eyey, random(eyeW), 20);
  ellipse(eye2, eyey, random(eyeW), 20);
  //mouth
  noFill();
  strokeWeight(6);
  stroke(0);
  arc(349, 320, random(smileW), 40, 0, PI);
  //nose
  strokeWeight(3);
  stroke(0);
  arc(349, 280, random(noseW), 20, 0, PI);
pop();
  //quote
  // textFont(myFont);
  fill(0);
  textSize(35)
  text('"Beacause one face does not fit all"', 350, 90)
  textSize(20)
  text('Roel Roscam Abbing, Peggy Pierrot and Femke Sneltings (2018)', 350, 130)
  textSize(15)
  text('(Hit enter to return to the standard emoji)', 350, 438)
  textAlign(CENTER)
}

function staticEmoji(){
  let lines = ('No face is the same\n So why should one emoji represent everyone\n Try to hit the spacebutton')
  // textFont(myFont);
  fill(0);
  textSize(30);
  textAlign(CENTER);
  textLeading(40); //space between the lines
  text(lines, 350, 80);
  // cirlce
  noStroke();
  fill(255, 244, 41);
  ellipse(350, 270, 200)
  //mouth
  noFill();
  strokeWeight(7);
  stroke(50,40,50);
  arc(349, 300, 100, 80, 0, PI);
  //eyes
  noStroke();
  fill(50,40,50);
  ellipse(eye1, eyey, 30, 25);
  ellipse(eye2, eyey, 30, 25);
}

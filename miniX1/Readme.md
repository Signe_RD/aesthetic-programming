## **ReadMe for MiniX1** ##
[See my project here!](https://signe_rd.gitlab.io/aesthetic-programming/miniX1/)

[See my code here!](https://gitlab.com/Signe_RD/aesthetic-programming/-/blob/master/miniX1/sketch.js)

![picture 1](dag.png)
![picture 1](nat.png)

**What have I produced?**
I have produced a drawing of a field with a tree, some clouds and a sun (see the picture 1 above). There is a button with a question in the sun "night?" and if you click it the background will get darker, the sun will turn into a moon and some bats will appear (see picture 2). The tree is made of three rectangles that are filled with two different brown colors, and six ellipses filled with two different green colors. The grass, clouds and bats are pictures. The button "night?" changes the background and load the picture with the bats while not loading the other pictures. 


**How would you describe your first independent coding experience (in relation to thinking, reading, copying, modifying, writing code, and so on)?**
This was my very first coding experience. I found it fun, frustrating, annoying and exciting! I started of with an idea of what I wanted to do and I did it more or less, but it was hard to figure out how to do anything in the beginning. The more references I read from the p5.js website the easy it got. But because programming/coding is new language, I had to read some of the references multiple times to understand them. I tried to write all the code myself rather than copying to get a better understanding of the code. When I had to modify some code the screen just went black sometimes, and that was very frustrating, but eventually I started to learn what I did wrong. I had a lot of problems with the button. I couldn't fiugre out how to make it go back to the "day" if you clicked it again, and I also coludn't get it to change colors. I also had to figure out where to write the different lines of code. What needed to only run once (be under function setup) and what needed to run over and over again (be under function draw).

**How is the coding process different from, or similar to, reading and writing text?**
You have to think a lot about how you write when you are writing code rather than text. When you write code, the smallest mistakes affects the entire code and the program won't run. While the smallest mistake won't mean anything for the text. Both writing coding and text recquire syntax. If the coding syntax is wrong the program won't run, and if the syntax in text (sentences) is wrong it may not make any sense.

**what does code and programming mean to me, and how does the assigned reading help you to further reflect on these terms?**
I see code and programming as an essential part of the field we study and the world we live in. I think it is important for digital designers to understand code and programming to understand what the possibilities and limitations are. I find code and programming very interesting and it is something I really want to learn. The assigned reading definite helped me understand some of the terms, and the p5.js references helped a lot too. 

**References:**
- Soon Winnie & Cox, Geoff, "Getting started", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 27-48 
- Video: Daniel Shiffman, Code! Programming with p5.js, The Coding Train. Available at https://www.youtube.com/watch?v=yPWkPOfnGsw&list=PLRqwX-V7Uu6Zy51Q-x9tMWIv9cueOFTFA&index=2 

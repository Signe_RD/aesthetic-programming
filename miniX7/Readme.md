## **ReadMe - miniX7** ##

[See my project here!](https://signe_rd.gitlab.io/aesthetic-programming/miniX7/)

[See my code here! (sketch)](https://gitlab.com/Signe_RD/aesthetic-programming/-/blob/master/miniX7/sketch.js)

[See my code here! (class)](https://gitlab.com/Signe_RD/aesthetic-programming/-/blob/master/miniX7/people.js)

![](people.png)
![](people1.png)
![](people2.png)


### **Describe how does/do your game/game objects work?** ###

So I have decided to start writing this ReadMe before finishing my code/program, which isn't something I normally do - so we may see how it goes. I have chosen to take my starting point for this exercize for this miniX7 in [Daniel Shiffmans snake game](https://thecodingtrain.com/CodingChallenges/003-snake-game-p5.html). I wanted to create a game that focus on the need for social interactions after corona, so that the "snake" consists of people that picks up other people, so that they create a line of people "holding hands" - to symbolize how much we all miss social interactions. My idea was that you see all the people from above, and to begin with they are grey (to symbolize sadness and loneliness) and then when you pick a friend up you and the friend(s) change to party light because you are happy and having fun, and your are going to get a beer together. 

I have rewritten and changed Shiffman's code to fit my program, but I have also rewritten it in the way we have learnt to write/work with code (he does it in a slightly different way) to really understand all the code. I have written all my code self - no copying - for the same reason. To begin with I had some problems changing the code and understand it all, but eventually I got it. I have also created a counter for how many friends the person has picked up from the grass, this counter is made from the sample code on the tofu game by Winnie Soon [(sample code)](https://gitlab.com/aesthetic-programming/book/-/blob/master/public/p5_SampleCode/ch6_ObjectAbstraction/sketch.js).

### **Describe & draw how you program the objects and their related attributes, and the methods in your game** ###

In my program I have one class = the people. The people is created from ellipses and they are seen from above. What does it mean that the people is a class? well this quote from Soon & Cox describes it very well: _"A class specifies the structure of its objects’ attributes and the possible behaviors/actions of these objects. Class can therefore be understood as a template for, and blueprint of, things."_ (soon & Cox p. 153). Here we can see how the class is the blueprint for all the people and it contains/or describes which attributes and which behaviors all the people can do.

When creating objects it is important to look at what it is you want theese objects to cointane and which properties they should have. To do that you can start out with looking at what it is you want them to do (that was what I did). I wanted them to be able to move around on the x and y axises when the user press specifics keys (left/right/up/down-arrow). I also wanted them to be able to pick their friends up and "hold hands" with them, this is tells something about how they interact with other objects. After looking at what the objects could do, I looked at which properties I wanted them to have. I wanted them to be a specific form and size, 30*30 in size (this can be seen in line 5 in sketch.js , this variable is also used to decide how much the people moves each time), they are ellipses to look like heads from above. The objects should also have a colour to reprensent their feelings (grey for lonely and party colours for happy). 


Here is an illustration of the objects properties and behaviors: 

|  | class |
| ------ | ------ |
| **properties** | people |
|  | ellipses (30*30) |
| | emotions colours |
|  | speed |
| ------ | ------ |
| **behavior** | move |
|  | pick up friends |
|  | death/party crash |
|  | show |
|  | move around |


### **Connect your game project to a wider cultural context, and think of an example to describe how complex details and operations are being “abstracted”?** ###

My game can be connected with the covid-19 pandemi, and how we all are looking forward to be more social and see everyone agian. The way my game represent this is that it is supposed to be seen as a person that is going around picking up all his friends to go get a beer - this is for many people a dream scenario, being able to have a giant garden party with all your friends and all the beers that you can possible drink ;) 

Why is it important to talk about abstraction and what there is abstracted. Well Soon & Cox has this quote that is great statring point to understand this: _"Abstraction is one of the key concepts of “Object-Oriented Programming” (OOP), a paradigm of programming in which programs are organized around data, or objects, rather than functions and logic"_ (soon & Cox p. 145). Here you can see how in OOP abstraction is used to describe and decide what data and how this data is used, and how it isn't about functions that draw on some specific data, but how the data is stored in obejcts and how this objects can behave. 

When talking about abstraction an important think to look into how some details will be left out when you turn a real world object into an object in a computer program (Soon & Cox p. 145). This can also be seen in my program where the people (the object) are "reduced to" (if you can say so) only having a few properties that dosen't represent all the properties real humans have. They can also only do a few actions/behaviors where real humans can do alot of differnet actions. This also closely relates to the following quote: _"With the abstraction of relations, there is an interplay between abstract and concrete reality."_ (Soon & Cox p. 160), where it can be seen how you can in your programs reality can create and decide how different objects relate and how this may be different from how the same objects relate in the real world. 


**References**
- Daniel Shiffman, Code! Programming with p5.js on YouTube, The Coding Train. "6.2: Classes in JavaScript with ES6 - p5.js Tutorial". link: https://www.youtube.com/watch?v=T-HGdc8L-7w (last accessed 26/03/21)
- codingtrain (Daniel Shiffman). "The Snake Game". link: https://editor.p5js.org/codingtrain/sketches/avh_VGx69 (last accessed 26/03/21) 
- Daniel Shiffman. "The Snake Game".https://thecodingtrain.com/CodingChallenges/003-snake-game-p5.html (last accessed 26/03/21)
- https://gitlab.com/aesthetic-programming/book/-/blob/master/public/p5_SampleCode/ch6_ObjectAbstraction/sketch.js (last accessed 26/03/21)
- https://www.youtube.com/watch?v=TgHhEzKlLb4 (last accessed 27/03/21) 
- Soon Winnie & Cox, Geoff, "Getting started", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 143-164 




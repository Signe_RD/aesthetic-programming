## **ReadMe miniX10 - flowcharts** ##

Created by: [Erna Holst Rokne](https://gitlab.com/ernahrokne), [Sofie Juul Nisted ](https://gitlab.com/sofienisted), [Eva Sørensen](https://gitlab.com/eva_sorensen) & [Signe Regin Runge-Dalager](https://gitlab.com/Signe_RD) 


### **Short introduction to our ideas** ###

1. The first idea is a program that takes some quotes from rap songs, in which some offensive is said about women. We then load the text into the program and change the pronouns to he/his instead. 
2. The second idea is a game inspired by Bot or Not - a chat-based game where the user tries to figure out if its chatting with a real user or a bot posing as human. Our idea places two obscure-sounding article titles next to each other, where the user will guess which one of the articles is real. When the user chooses an option, a linked image of the real article pops up, for the user to read if interested. The program is meant to create reflection and critical thinking around awareness of fake news, bots and clickbait. We think people should be able to differentiate between real and fake information, and this game is meant as an exercise in that discipline.

**Flowchar for rap-text idea:**
![](f1.png)

**Flowchat for fake news-idea**
![](f2.png)

### **Questions to think about on our Readme** ###

**What are the difficulties involved in trying to keep things simple at the communications level whilst maintaining complexity at the algorithmic procedural level?**

The difficulties are mainly trying to summarize or boil down the most important technical steps in the process, and figure out which to leave out in the flowchart for it to not become super complicated. When starting to make a flowchart (before making the program) it can seem more simple than the program actually will be - it is important to remember that it's only the main steps and procedures that are being illustrated in the flowchart, so that the communication concerning the idea is clear. Overall, it’s really difficult to point out the main processes of a complex program, but when it is done it gives a much better understanding of the program, and flowcharts can be a great tool for this: “Flowcharts allow us to "see" soft- ware in ways that are otherwise impossible” (Ensmenger 346). 

**What are the technical challenges facing the two ideas and how are you going to address these?**

Some of the technical challenges we probably are going to face with our two ideas is using and getting data from JSONfiles and APIs, and writing the right paths to get the data.  

We want to create some if-statements for the rap-text idea where specific words that relate to females/women get changed to different predetermined ones, this is definitely going to be a challenge for us. We aren’t sure how to do this yet, so we will have to try it and read a lot about it before we are going to get it to work. The code for the music-idea appears to be pretty easy and short, and a lot of the needed syntaxes we already know, but we won't let this fool us, we know it is going to be a challenge.
For the fake-news idea the code syntax is also pretty easy and something we already know. However, we think it could become tricky for us to get the articles to be displayed in the two textboxes in a random fashion, so that the user won’t be able to tell which is which from their respective positions.


**In which ways are the individual and the group flowcharts you produced useful?**

The flowcharts concerning the group project are useful for us when communicating the ideas and structure of the programs, and work as a good starting point when working with many people on a project, as explained by Soon and Cox: “If tasks need to be sub-divided among a group, for instance, flowcharts can be used to identify how a smaller task can be linked to others without losing site of the bigger picture.” (215)

The individual flowcharts are definitely useful for gaining a better understanding of your own program and getting a good overview of the order of the flow of the program - miscommunication can be the cause of errors, so it’s great to have a shared overview. When simplifying the syntax of a program, one also gets a better sense of the important aspects of the program.

(Eva) For me, making the flowchart was a little difficult because I thought many of the progresses in the program were happening all at once. But when splitting it up and studying the temporal order of my program, it improved my understanding of the source code a great deal. 

(Erna) I chose minix7 (the game with objects) for the flowchart, which was a little complex, but as I figured out which parts and syntax to include, I realized that the game wasn’t as complicated as I had thought. I think flowcharts are great for this organization and understanding of what you are making - especially in this context when you are new to coding.

(Signe) I chose my minix7 as well as Erna. I found it a bit hard to figure out where to start and what to focus on in the beginning, but eventually I figured it out. I had a hard time figuring out how to connect the object into my flowchart. But overall I think that flowcharts are a great tool to understand and communicate different ideas. 

(Sofie) I chose my miniX8 even though it wasn’t the most complex one I have made, but I have a better understanding of it than the one about APIs. I worked with other people for both, and it really helps my understanding to break the process down into steps. 

### **My own flowchart for miniX7** ### 

I chose to work on my miniX7 (the game with objects) because I found it the most technical and difficult to communicate through a flowchart. 

![](s1.png)
![](s2.png)
![](s3.png)


### **References:** ###

- Bot or Not game: https://botor.no/ (last accessed: 23.04.21)
- Soon Winnie & Cox, Geoff, "Algorithmic procedures", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 211-226
- Nathan Ensmenger, “The Multiple Meanings of a Flowchart,” Information & Culture: AJournal of History 51, no.3, 2016, pp. 321-351


let circleW = [200, 150, 220, 120, 190];
let circleH = [200, 160, 250, 170, 230];
let eye1 = 310;
let eye2 = 390;
let eyey = 245;
let eyeW = [35, 25, 30];

//I tried to get the colors to work for a long time, but I couldn't figure it out,
//but I decided to keep them this way to remember what it was I tried to do.
let r = [141, 198, 224, 241, 255];
let g = [85,134, 172, 194, 219];
let b = [36, 66, 105, 125, 172];

let smileW = [50, 80, 60];
let noseW =[10,20,30]

let myFont;
function preload() {
myFont = loadFont('font1.otf');
}

function setup() {
createCanvas(700, 500);
frameRate(5);
}

function draw() {
background(200);
if (keyIsPressed){
//face
strokeWeight(1);
stroke(0);
fill(random(r),random(g),random(b));
frameRate(3);
ellipse(350, 270, random(circleW), random(circleH));
//eyes
strokeWeight(1);
stroke(0);
fill(255)
ellipse(eye1, eyey, random(eyeW), 20);
ellipse(eye2, eyey, random(eyeW), 20);
//mouth
noFill();
strokeWeight(6);
stroke(0);
arc(349, 320, random(smileW), 40, 0, PI);
//nose
strokeWeight(3);
stroke(0);
arc(349, 280, random(noseW), 20, 0, PI);

//quote
textFont(myFont);
fill(0);
textSize(45)
text('Beacause one face does not fit all', 350, 100)
textSize(15)
text('Roel Roscam Abbing, Peggy Pierrot and Femke Sneltings (2018)', 350, 130)
textAlign(CENTER)

} else {
textFont(myFont);
fill(0);
textSize(45)
text('No face is the same', 350, 100)
textSize(35)
text('Try to press any key', 350, 140)
textAlign(CENTER)

// cirlce
strokeWeight(1);
stroke(51);
fill(255, 244, 41);
ellipse(350, 270, 200)
//mouth
noFill();
strokeWeight(7);
stroke(50,40,50);
arc(349, 300, 100, 80, 0, PI);
//eyes
noStroke();
fill(50,40,50);
ellipse(eye1, eyey, 30, 25);
ellipse(eye2, eyey, 30, 25);
}
}

function setup() {
createCanvas(700,600);
}

function draw() {
  background(64, 114, 100, 80); //transparency --> the others fades over time
  if (mouseX>280){ //condition for clock throbber
  frameRate(1);
  drawClock2();
} else if (mouseX>200){ //condition for com throbber
  frameRate(12);
  drawElements();
  drawElements1();
  drawElements2();
} else { //standard
  frameRate(7);
  drawClock();
  drawElements();
  fill(60);
  textSize(20)
  text('Try moving your mouse around',220,60);
}

function drawClock (){ //function for standard clock throbber
  let num = 12; //local variable
push();
  translate(width/2, height/2);//moves the origin
  let cir = 360/num*(frameCount%num); //calculate rotate each ellipse 40 degree
  rotate(radians(cir)); //rotate clockwards
  noStroke(); //no border
  stroke(50);
  line(0,0,70,70);
pop();
}

function drawClock2 (){ //function for clock throbber
  let num = 12; //local variable
push();
  translate(width/2, height/2);//moves the origin
  let cir = 360/num*(frameCount%num); //calculate rotate each ellipse 40 degree
  rotate(radians(cir)); //rotate clockwards
  noStroke(); //no border
  strokeWeight(3);
  stroke(0);
  line(0,0,120,120);
pop();
}

function drawElements (){ //function 1 for com throbber
  let num = 12; //local variable
push();
  translate(width/2, height/2);//moves the origin
  let cir = 360/num*(frameCount%num);
  rotate(radians(-cir)); //rotate backwards
  noStroke(); //no border
  fill(50);
  ellipse(150,0,30); //30 is the radians of the ellipse
pop();
}

function drawElements1 (){ //function 2 for com throbber
  let num = 10; //local variable
push();
  translate(width/2, height/2);//moves the origin
  let cir = 360/num*(frameCount%num);
  rotate(radians(cir)); //rotate backwards
  noStroke(); //no border
  fill(80);
  ellipse(100,0,30); //30 is the radians of the ellipse
pop();
}

function drawElements2 (){ //function 3 for com throbber
  let num = 8; //local variable
push();
  translate(width/2, height/2);//moves the origin
  let cir = 360/num*(frameCount%num);
  rotate(radians(-cir)); //rotate backwards
  noStroke(); //no border
  fill(20);
  ellipse(50,0,30); //30 is the radians of the ellipse
pop();
}

}

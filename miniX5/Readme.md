## **ReadMe miniX5** ##

[see my project here! (open console.log for full experience)](https://signe_rd.gitlab.io/aesthetic-programming/miniX5/)

[see my code here!](https://gitlab.com/Signe_RD/aesthetic-programming/-/blob/master/miniX5/sketch.js)

![](dot1.png)
![](dot2.png)

### **Description and rules + the rules roles** ###

What are the rules in your generative program? Describe how your program performs over time? How do the rules produce emergent behavior?

There are 2 rules in my generative program, and they are: 

1. Draw black ellipses randomly with a predetermend x value and a random y value 
2. Draw white ellipses randomly with a predetermend x value and a random y value 
3. Write Lars in `console.log` if `random(7)>1`
4. Write Woman in `console.log` if `random(7)<1`

My program consits of ellipses in two colors (black, and white), that seems to appear random all over the canvas. The ellipses appears quite slow, and that is to give the user to think about the randomness in the placement, before it is revealed that it isent that random. The ellipses is made in a for loop for`(let x=0;x<width;x+=spacing)`, where the spacing between them is predetermend and eventually the ellipses will "create" lines. The reason the ellipses eventually create lines is to potrait the idea of randomness and if any thing really is random. So the user/watcher will start out with a canvas where ellipses will pop up as what would appear to random places, but as the program runs the ellipses create lines and the idea of the random placement will fade away. 

I have also used the `console.log` as a part of my work. I wanted to incorporete the problems with gender equality among the CEOs in Denmark. In Denmark there are more CEOs named the specific name Lars than there are female CEOs. I find this highly disturbing and problematic and I wanted to incorporete it in my work, by writing _Lars_ and _Woman_ in the console.log. The progam writes Lars if the program picks a random number between 0 and 7 that are higher than 1, and writes Woman if the number is lower than 1 - to symbolise this problematic. The two colored ellipses also symbolize this somewhat, but not as specific, in a way you can see the black ellipses as Lars CEOs and white as female CEOs, but the numbers wont add up. 

The rules in my program control where different (black and white) ellipses are drawn on the y-axes. They also control when there is written Lars or Woman in the console.log. There is two conditional statements in the for loop that draws the ellipses (see line 11-29). The conditional statements control when there is drawn a black or white ellipses, and when there is written Lars or Woman in the console.log. 

### **How the miniX help you understand the idea of "auto-generator"** ###

Draw upon the assigned reading, how does this MiniX help you to understand the
idea of “auto-generator” (e.g. levels of control, autonomy, love and care via rules)? Do you have any further thoughts on the theme of this chapter?

I want to start out with looking at the quote by LeWitt _““The idea becomes a machine that makes the art,” as LeWitt explains."_ (soon & cox 2020 p.124). I think this quote describes the idea about generative systems very well, and this is kind off what I wanted to work with, I wanted to create some art, and I wanted the computer to create it from some simple rules. I believe this is one very important part about the idea of "auto-generator", that computers automatically create/generator something. 

Generative art or systems draws on some of the same ideas as living systems. In the following quote the idea of generative art is changeable to specific conditions: _"it would seem that generative systems promise something similar as a way to envisage existing systems as changeable or adaptive to conditions"_ (soon & cox 2020 p.136). I can draw this to my work, generative art and this exercise by looking at the idea of rules. We were told to come up with at least two rules and create a program/work from these. These rules are the conditions that control when something is run and when something is not, in my work - when a white or black ellipse is drawn and when it writes Lars or Woman in the console.log. 

I wanted to create a generative art piece that focuses and asks question around randomness, and how random anything really is. I wanted to look into the idea of pseudorandomness, and how it can give an idea of randomness without it ever really being random. When a computer generates something that is supposed to seem random, but really isn't it is what Montfort will refer to as pseudorandomness. Montfort describes the psuedo part of the word as: _"The somewhat dismissive-sounding “pseudo” refers to the fact that a deterministic process (a computer program) is being used to generate sequences of numbers that appear to be uniformly distributed."_ (Montfort 2012, p.130). Here can be seen that pseudo relates to how the computer program generate number sequences that appear random but isent. That is also what I wanted to do with the idea that the placement of the ellipses seems random, but really isn't. But then how can you know wheter it is true randomness or pseudo, Montfort describes this here  _"But eventually, for long enough sequences, the deterministic nature of a pseudorandom number generator will be unmasked"_ (Montfort 2012, p.130), here it can be seen that the program needs to run a long time before the pseudo part starts to appear and the "fake" randomness is shown. Agian this is something I have tried to implement by letting the program take a long time to run, so that it takes a "long" time before the lines of ellipses appear.

### **References** ###
References that I have looked at for inspiration:

- Soon, Winnie & Cox, Geoff. Aesthetic Programming: A Handbook of Software Studies. London: Open Humanities Press, 2020. p. 123-140. 
- Nick Montfort et al., “Randomness”, in Montfort et al, 10 PRINT CHR$(205.5+RND(1)); : GOTO 10, Cambridge, MA:MIT Press, 2012, pp. 119-146 
- Shiffman, Daniel - The coding train. uploaded 11 sep. 2015. "Nested Loops - p5.js Tutorial". https://www.youtube.com/watch?v=1c1_TMdf8b8&t=160s
- John P. Bell. "Asterisk Painting". http://www.johnpbell.com/asterisk-painting/ (last accessed 14/03/2021)
- Christopher Strachey, 1962. "Love Letters" ([link to reimplement by Nick Montfort](https://nickm.com/memslam/love_letters.html))
- Rosenbæk, Katrine, 6/10/2020. "Hvis du gerne vil være topchef, skal du overveje at skifte navn til Lars". https://www.femina.dk/liv/hvis-du-gerne-vil-vaere-topchef-skal-du-overveje-skifte-navn-til-lars (last accessed on 14/3/2021).

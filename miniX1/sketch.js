let img;

function preload() {
  img = loadImage('cloud.png');
  img1 = loadImage('bat.png');
  img2 = loadImage('grass.png')
}

function setup() {
  createCanvas(700, 500);
  print("hello world")
  background(135, 206, 250);

  //button to change backgground
  let a=color(255, 255, 0)
  button = createButton('night?');
  button.style('background-color',a);
  button.position(590, 60);
  button.mousePressed(changeBG);

  //sun
  fill(255, 255, 0);
  ellipse(620, 70, 110, 110);

  // clouds
  image(img, 300, 90, 130, 100);
  image(img, 0, 30, 130, 100);
  image(img, 400, 30, 80, 60);
  image(img2, 0, 440, 700, 60);
}

function draw() {
  //first thrunk
  let c = color(125, 73, 5);
  fill(c);
  rect(200, 200, 30, 295);
  //second thrunk
  c = color(156, 93, 42);
  fill(c);
  rect(180, 210, 20, 285);
  //third thrunk
  fill(c);
  rect(230, 210, 20, 285);

  //first cirkel
  c = color (0, 102, 0)
  fill(c)
  ellipse(180, 180, 100, 100);
  ellipse(260, 190, 100, 100);
  ellipse(220, 250, 100, 100);

  //second cirkel
  c = color(0, 150, 0);
  fill(c)
  ellipse(160, 220, 90, 90);
  ellipse(220, 170, 90, 90);
  ellipse(260, 240, 90, 90);
}

function changeBG(){
changeBG=background(0, 0, 76);
//moon
fill(255, 255, 204);
ellipse(620, 70, 110, 110);

//bats
image(img1, 310, 30, 250, 200);

}

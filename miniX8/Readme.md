## **_Corona guidelines that will absolutely, 100% work_** ## 
**MiniX8 - E-Lit** 

Erna Holst Rokne & Signe Regin Runge-Dalager 

[See our project here!](https://signe_rd.gitlab.io/aesthetic-programming/miniX8/)

[See our code here!](https://gitlab.com/Signe_RD/aesthetic-programming/-/blob/master/miniX8/sketch.js)

[See our JSON file here!](https://gitlab.com/Signe_RD/aesthetic-programming/-/blob/master/miniX8/coronathings.json)

![](corona1.gif)
![](corona.png)

### **description** ###
**Provide a title of your work and a short description.**

**Title:** _Corona guidelines that will absolutely, 100% work_

We have created a program that shows different guidelines to prevent corona. It states that “If you __, and __ you won’t get corona”, and includes very absurd statements (that definitely won’t work) that are supposed to be corona guidelines. We took inspiration from ‘Mexicans in Canada’, created by Amira Hanafi in 2020, where she takes an ironic spin on a political matter. As the corona-pandemic has been raging on through the last year, we have found it difficult to keep up with the current guidelines, and it seems as though they change every day. We have come to a point where we are no longer sure what actually works and not, and we wanted to present our feelings towards this issue with our program. Here are some examples of our absurd guidelines: carry 15 rolls of toilet paper at all times, shower in vodka two times a day etc. Hope you enjoy it! 

**Technical/syntaxes** 

We have created a program with some text where some of it changes randomly. We have used different syntaxes to create the changing text. We have also written and created our own JSONfile that contains the data we want to pick randomly, in the form of these absurd statements. To use the data from the JSONfile we have created a variable (corona) that contains the path of how to get the data (see line 21). We have used two variables i and j to pick a random number from 0-5 and 6-10, we use these variables in the variable statement to pick a random sentence from the JSONfile. To create the interactive element of the program, we have used the function mousePressed, where the user can “stop” and “start” the random function with their mouse.  

## **Analyze** ###
**Analyze your own e-lit work by using the text Vocable Code and/or The Aesthetics of Generative Code**

If we look at this quote from Cox et al.: “Our argument is that, like poetry, the aesthetic value of code lies in its execution, not simply its written form.” (Cox et al., p.1), it can be seen that they not only mean that the value of the poetry (which we may look at as E-lit in our case) is more than just the code, or the execution - it is a mix of both. This relates very well with our idea of how our program/work is “best” represented when it is executed and the user interacts with it, but if the user also reads the code and how it is written it may give an even better understanding of the work and the message, we are trying to send. And this relates closely with what the following sentence after the quote is: “However, to appreciate generative code fully we need to ‘sense’ the code to fully grasp what it is we are experiencing and to build an understanding of the code’s actions.” (Cox et al., p.1). Here we can see how Cox et al. focus on the work as a whole and not only as one part (for an example the written code). 

**Reclection on our work in terms of Vocable Code** 

In the text ‘Vocable Code’ by Geoff Cox and Alex Mclean, they argue that “programs can be called into action as utterances that have a wider political resonance; they express themselves as if possessing a voice” (19). In our program we had a political agenda, and wanted to express it in a humorous way. The sarcasm is very clear, and we believe that it is apparent for people what the message is. Our program consists of technical syntaxes ordering the code around, but looking into it, one can see a close connection with the meaning of the program. Especially as the code asks for a random statement when looping, it is very much like how we perceive the corona guidelines sometimes - that they seem random, strange and sometimes out of place. In the text, they also mention how “meaning is organized out of differences between elements that are meaningless in themselves” (Cox and McLean 19), and we think this connects well with our program. Looking at the code, one can get the impression of unseriousness, as the text calls for statements that are ‘chosen’ by the computer. We also think this resonates well with the analogy to poetry, whereas we tell the computer to use this seemingly random code, to in fact express a political view or utterance that makes sense as a whole.

**References:**

- Daniel Shiffman, “10.2: What is JSON? Part I - p5.js Tutorial” (2017). Link: https://www.youtube.com/watch?v=_NFkzw6oFtQ&list=PLRqwX-V7Uu6a-SQiI4RtIwuOrLJGnel0r&index=3 (last accessed 11/04/21) 
- Daniel Shiffman, “10.2: What is JSON? Part II - p5.js Tutorial” (2017). link: https://www.youtube.com/watch?v=118sDpLOClw&list=PLRqwX-V7Uu6a-SQiI4RtIwuOrLJGnel0r&index=5 (last accessed 11/04/21) 
- Amira Hanafi. "Mexicans in Canada". 2020. http://amiraha.com/mexicansincanada/?fbclid=IwAR2AqJ9GQNhERYLmJdzS2QlfxdqUIZh6f3zX9GSN5ukIeVkvCvMA66AEhPc (last accessed 11/04/21)  
- Allison Parrish, “Text and Type” (2019) https://creative-coding.decontextualize.com/text-and-type/ 
- Soon Winnie & Cox, Geoff, "Getting started", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 165-186 
- Geoff Cox and Alex McLean, “Vocable Code,” in Speaking Code (Cambridge, MA: MIT Press,2013), 17-38. 					
- Geoff Cox, Alex McLean, and Adrian Ward, “The Aesthetics of Generative Code,” Proceedings of Generative Art Conference, Milan (2001). 
- https://www.dafont.com/roboto.font (last accessed 11/04/21)

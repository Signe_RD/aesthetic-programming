## **MiniX9 - Elit** ## 
**Eva Sørensen & Signe Regin Runge-Dalager**

[See our project here!](https://signe_rd.gitlab.io/aesthetic-programming/miniX9/)

[See our code here!](https://gitlab.com/Signe_RD/aesthetic-programming/-/blob/master/miniX9/sketch.js)

![](trump.png)

**What is the program about? Which API have you used and why?**

Our program is strongly influenced by the setup of twitter. With the use of our API, a quote Trump has said (not written on twitter necessarily) is shown. The API we have used is “What Does Trump Think API”. When we were searching and looking around for different API’s and saw this one, we immediately got some ideas for a program and therefore we went with this one. First of all, it was an obvious idea for us to think about fake news and how technology plays a big role in the modern world's news coverage and we tried to express this in our program. Lastly, we also thought this would be fun and interesting to work with. 

To add, in January 2021 Trump was suspended from Twitter, because he inflamed people to riot in the congress. This started, or added to, a discussion about fake news and freedom of speech. Now there’s talk about Trump opening his own social media, which again leads to disagreements in this particular discussion; very much depending on which side you’re on in the political discussion in The United States right now. 

**Can you describe and reflect on your process in this miniX in terms of acquiring, processing, using, and representing data? How much do you understand this data or what do you want to know more about? How do platform providers sort the data and give you the requested data?**

We started looking at different APIs in the beginning, to get an idea of which there was and what we wanted. We found the [tronald dump](https://www.tronalddump.io/) API quite quick and we found it really funny. We wanted to make fun of Trump's quotes and start a debate about fake news and censorship on social media. There was a guide to use the API, but we couldn't find the base url and we searched for quite a long time, but we then ended up with finding a different API that also had quotes by [Donald Trump](https://api.whatdoestrumpthink.com/api/v1/quotes), so we ended up using that one. They also had a guide for how to use the API, so it was really easy to use, and after a few tries we found out how to get the url with the right path for the data we wanted. The API is free and does not require a API key or something like that, so that was nice.  

![](2.png)
![](1.png)

The JSONfile is pretty simple to read and understand, they have used easy and relatable words to describe the different arrays. We wanted the non personalized quote, so our path to get the right data ended up being: `messages.non_personalized[i]`, and then we made it into a variable that was easier for us to use: `getQuote = data.messages.non_personalized[i];`. 

We understand the data quite well, as it is pretty simple to understand and read, there are two arrays one with personalized quote and one with non personalized. The personalized quotes are about different people, but it doesn't say who the original is about, but they all start with a verb saying something about someone. The non personalized quote is a “whole” quote that Trump has said at some point, and this is the data we wanted, because we wanted quotes we could use without adding something. But we don’t know where the API gets its quotes from, and that could be funny to learn about. 

**What are the power relations in the chosen APIs? What is the significance of APIs in digital culture?**

As mentioned, we don’t know where the API gets the quotes from - so we haven’t exactly been critical in our selection of API. And if we needed to use this program for something else, we would probably either look into the content of the API and its origins, or choose another. But we chose to use it since it was really simple and easy to use; only needed to input the url in our program and it contained exactly the data we wanted. 

We have talked about the significance of API’s in digital culture in this week's lectures. Here we actually talked about being critical about API’s and even more about data and information we’re given. This relates closely with what Soon & Cox writes about here: _“(...) asking whether something like data is valid or accurate, but also to questioning how it is deemed to be valid or accurate in the first place.”_ (Soon & Cox, p. 205), where they also talk about the importance of questioning where the API gets its data from, which is something we would like to know. 
	
Also the importance of  how neutral the API may be. The API we use has collected a bunch of quotes from Donald Trump, but which quotes and why those quotes we don’t know, we can guess, and since all of them are funny and somewhat stupid/fake news we guess that the API creator have ignored the more serios quotes from Trump. This is also one of the problems Soon & Cox write about, as it can be seen in the following quote: _"To que(e)ry data in this way throws into further question how data is collected, stored, analyzed, recommended, ranked, selected, and curated in order to understand the broader social and political implications, (...)”_ (Soon & Cox, p. 207). 
								
**Try to formulate a question in relation to web APIs or querying/parsing processes that you would like to investigate further if you had more time.**

We found a really easy accessible API that had all the data that we wanted, but if we had the time, it could have been fun and interesting to work with an API that required API keycode, engine ID and so on, to get a better understanding of it. 
Some questions we find interesting and would have liked to investigate further: 
- Where does the data come from (for our specific API) and how is it collected? 
- In what other scenarios is this API used and for what? 



**References:**
- "What Does Trump Think". link: https://whatdoestrumpthink.com/api-docs/index.html?javascript#introduction - last accessed 16/04 
- https://api.whatdoestrumpthink.com/api/v1/quotes (API) - last accessed 16/04 
- https://www.tronalddump.io/ (The picture we have used in our program)  - last accessed 18/04 
- Mathias Schilling. "Tronald Dump". link: https://creative-coding.decontextualize.com/text-and-type/ (to get the text to work) - last accessed 16/04 
- https://www.cnbc.com/2021/04/07/capitol-riot-congress-members-sue-trump-giuliani-proud-boys-oath-keepers-.htm - last accessed 16/04 
- Soon Winnie & Cox, Geoff, "Getting started", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 187-210


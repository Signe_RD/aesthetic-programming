## **ReadMe miniX4** ##

[see my project here!](https://signe_rd.gitlab.io/aesthetic-programming/miniX4/)

[see my code here!](https://gitlab.com/Signe_RD/aesthetic-programming/-/blob/master/miniX4/sketch.js)

![](1.png)
![](2.png)
![](3.png)

### **Click Me** ###

I have chosen to name my work "Click Me" as a direct reference to the click me button that appears after the user has submitted their name. The phrase "Click Me" is the name of the work and the name of the button because it is meant to be seen as a luring phrase to get the user to click. And to make it even more playful and luring the name of the buttons change to "no click me" after the first button. The buttons is supposed to symbolize how we as humans have a need to keep on clicking, we need to know where we end up, we don't wanna miss out. The user starts by submitting their name without any explanation on why to get the user to question where their data ends up and why they need to give it. When the user reaches the final page, they will experience that the data they started out giving (their name) will appear with a note about what the program is about. 

### **Description of my program** ### 

I have created a program using a few new functions, such as buttons (which I have worked with in may first miniX, but I understand their syntax and code much better now), I have also used the `let lines` to create some text. Be using the let lines syntax it becomes so much easier to write a paragraph of text with a chosen distance between the lines, instead of writing `text()` for every single line of text, so I am definitely going to use this syntax again. When creating my buttons (there is 8 in total), I wanted the buttons to change size and placement randomly, I did it by using the random syntax in the `button.positions()` and `button.size` syntax. I also wanted to create a counter to count every time someone has clicked on the last button, to show the users that they are not alone with their _addiction of clicking buttons_ ;). I have created this in line 88 to 90, where it can be seen that every time function `text1()` is called on (by clicking the last button) it picks a random number between 0 and 1000. I tried to make it increase by 1 every time the last button was clicked, but I chose to go with the random number to create an idea of many people using the program, and to create a feeling of not being the only one who clicked through all the buttons, even tough the user may be. I have also created an input box on the first page of the program (see line 8-9), where the user can write their name (or in principle they can write anything they want, but the program encourages the user to write their name), and the value that the user writes in the input box isn't used until the final page where the program writes "Hi _name_ ". 

### **"Capture all"** ###

The program takes the user's data in the form of their name and how many buttons they click and uses it. But as the user you have no idea what the program uses your data to or who uses it, this relates to Soon & Cox's quote _"Few people know exactly which data is captured or how it is used"_ (Soon & Cox 2020, p.111), that shows that users is aware of which data they give up/is captured. This fit very well the idea of "capture all" where all we do as humans become data one way or another. In this program the users' data aren't really used to anything groundbreaking, but it shows how easily we give up data (ex. our name) without even thinking about it. 

I wanted my program to seem playful with the buttons changing size and placement and the idea of not knowing when something is going to happen. In the Transmediale call for works around the Capture All theme they write: _"Playful and participatory structures are purposefully used to render personal information traceable, social relationships exploitable and behavioral patterns recognizable"_ (https://archive.transmediale.de/content/call-for-works-2015, accessed 04/03/2021). And in a way (and this is probably to stretch it) my program can be seen as a way to get personal information (the user's name) and to learn about the user's behavioral patterns by looking at how many people that get to the final page, but I can't really look into this since my program doesn’t really count how many people. 

### **Cultural implications of data capture** ###

The idea with my program is to show how we as human beings constantly is experiencing FOMO and this also can relate to how we may click on buttons that we don’t necessarily know what will happen. The input box where the program asks the user to type in their name is meant to symbolize how willing we give up personal data, like our name, again with no idea what it is going to be used to. The user's name isn’t used until the final page, and this may confuse the user (which is the point ;)) and make them wonder where their data have ended up. 

As an user, you may not always know which people/actors that end up with your data, and how they use it. Mejias and Couldry comes with a list of possible actors _"(...) corporations, states, and various civic (activists,  journalists, etc.) and even non-state (terrorists, hackers) actors, all of which can produce, collect and analyse data for different purposes."_ (Mejias and Couldry 2019, p.2). What they try to say here is that we as users doesn’t necessarily knows where our data ends up, and by whom and how it is being used. I tried to show this in a small scale, by not telling or showing (until the final page), what I/the program needed the user's name for. 

Living in the time that we are, where so many things are online and digital we click so many buttons every single day. Soon and Cox writes about how clicking a button is a pretty simple action, and something we do quite often. They also mentioned how _"(...) a button is "seductive""_ (Soon & Cox 2020, p.99). This is also the principle that I tried to work with, the point that buttons are seductive and we want to know what happens when you click it. This also corresponds with how buttons online platforms and software invites the user to click the button, and how the user can feel like it is a meaningful interaction even though it has very few (or in my case only one (two if you also consider not clicking the button as a choice)) options (ibid. p.99). 

If the user stops clicking the "no click me" buttons the program loses its meaning, and the user will never reach the final page/message. So, this entire program runs on the idea that we as human beings need to know everything and we need to make sure we see everything. 

### **References** ###
References that I have looked at for inspiration:

- https://p5js.org/examples/dom-input-and-button.html 
- Daniel Shiffman, Code! Programming with p5.js on YouTube, The Coding Train. "8.6: Other Events and Inputs - p5.js Tutorial" Link: https://www.youtube.com/watch?v=HsDVz2_Qgow&t=1015s
- Daniel Shiffman, Code! Programming with p5.js on YouTube, The Coding Train. "7.4: Adding and Removing Objects - p5.js Tutorial" Link: https://www.youtube.com/watch?v=EyG_2AdHlzY&t=809s
- Soon, Winnie & Cox, Geoff. Aesthetic Programming: A Handbook of Software Studies. London: Open Humanities Press, 2020. p. 103-104. 


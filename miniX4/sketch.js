let count = 0;
let input, button, greeting;

function setup() {
  createCanvas(700,600);
  background(210,203,240);

  input = createInput();
  input.position(275,265);

  button = createButton('Submit');
  button.position(310,300);
  button.size(80,20);
  button.mousePressed(changeBG);

  greeting = createElement('h2', 'what is your name?');
  greeting.position(250, 180);

  textAlign(CENTER);
  textSize(50);
 }

function changeBG() {
  button.remove();
  input.remove();
  greeting.hide();
  fill(255);
  button1 = createButton('Click me');
  button1.position(160,200);
  button1.size(400,140);
  button1.mousePressed(changeBG1);
}

function changeBG1(){
  button1.remove();
  button2 = createButton('no click me');
  button2.position(random(20,400),random(10,500));
  button2.size(random(50,500),random(25,50));
  button2.mousePressed(changeBG2);
}

function changeBG2(){
  button2.remove();
  button3 = createButton('no click me');
  button3.position(random(20,400),random(10,500));
  button3.size(random(50,500),random(25,50));
  button3.mousePressed(changeBG3);
}

function changeBG3(){
  button3.remove();
  button4 = createButton('no click me');
  button4.position(random(20,400),random(10,500));
  button4.size(random(50,500),random(25,50));
  button4.mousePressed(changeBG4);
}

function changeBG4(){
  button4.remove();
  button5 = createButton('no click me');
  button5.position(random(20,400),random(10,500));
  button5.size(random(50,500),random(25,50));
  button5.mousePressed(changeBG5);
}

function changeBG5(){
  button5.remove();
  button6 = createButton('no click me');
  button6.position(random(20,400),random(10,500));
  button6.size(random(50,500),random(25,50));
  button6.mousePressed(text1);
}

function changeBG6(){
  button6.remove();
  button7 = createButton('no click me');
  button7.position(random(20,400),random(10,500));
  button7.size(random(50,500),random(25,50));
  button7.mousePressed(text1);
}

function text1(){
  button6.remove();
  background(210,203,240);

  //text
  fill(0);
  textSize(40);
  textAlign(CENTER);
  text('Hi',270,150);

  fill(0);
  textSize(40);
  textAlign(CENTER);
  text(input.value(),360,150);

  let lines1 = ('As humans we have a need to know what there is going to happen\n So if we see a button we feel a need to click it\n Because what if we miss out on something\n')
  fill(0);
  textSize(20);
  textAlign(CENTER);
  textLeading(30); //space between the lines
  text(lines1, 330, 250);

  let lines2 = ('But dont worry - you are not alone\n So many has clicked all seven buttons\n Only to get to this site')
  fill(0);
  textSize(20);
  textAlign(CENTER);
  textLeading(30); //space between the lines
  text(lines2, 330, 400);

  //counter
  let count = round(random(0,1000),0);
  textAlign(CENTER);
  textSize(30);
  text(count,330, 500);
}

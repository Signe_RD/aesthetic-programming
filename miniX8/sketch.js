let coronarules;
let corona
let myFont;
let myFontbold;
let val=0;

function preload(){
  coronarules=loadJSON("coronathings.json");
  myFont=loadFont('font.ttf');
  myFontbold=loadFont('bold.ttf');
}

function setup(){
createCanvas(windowWidth,windowHeight);
frameRate(40);
}

function draw(){
background(100,200,150);

  corona = coronarules.coronathings;

  let i = floor(random(0,5));
  let j = floor(random(6,10));
  let statement = "If you " + corona[i] + ",\n" + "and " + corona[j];

  textSize(25);
  fill(50);
  textFont(myFont);
  text(statement,50,20+(2*50));
  textFont(myFontbold);
  text("...you won't get corona",50,95+(2*50))

  }
function mousePressed(){
  val = val + 1;

  if(val%2!=0){
    noLoop();
  }else{
    loop();
  };
}

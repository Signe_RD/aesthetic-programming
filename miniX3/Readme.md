## **ReadMe miniX3** ##
[see my project here!](https://signe_rd.gitlab.io/aesthetic-programming/miniX3/)

[see my code here!](https://gitlab.com/Signe_RD/aesthetic-programming/-/blob/master/miniX3/sketch.js)

![Throbber](normal.png)

![Throbber2](com.png)

![Throbber3](clock.png)

![Mobilepay](mobilep.png)

### **My project (technically)** ###

I have created a throbber which technically consist of two throbbers rotating in different directions, one clockwise and one counterclockwise. The clockwise throbber consists of 12 lines rotating around the new origin created by the use of translate function. The counterclockwise throbber also consists of 12 elements but this time it 3 times 8 to 12 ellipses also rotating around the new origin. When moving your mouse around on the screen the throbbers changes. I have used different time-related syntaxes such as _push/pop, translate, frameRate_ and _rotate_. I have used push and pop in the two functions drawElements and drawClock where I use it to make sure it only runs the lines of code in between push and pop in a loop without affecting the other functions/syntaxes. I have used the syntax translate to change the origin in the center of the canvas, because I wanted the throbber to rotate around the center of the canvas. I have used the rotating syntax to get the lines and ellipses to rotate (or technically get the canvas to rotate) in different directions by writing rotate(radians(cir)) to get it to rotate clockwise and rotate(radians(-cir)) to get it to rotate counterclockwise. I have used the frameRate syntax to control how fast the frames changes, the default setting for frameRate is 60 frames pr. second. The frameRate changes depending on where the user is moving their mouse, the frames changes faster when holding the mouse on the counterclockwise throbber to show how computers can think and work really fast. At the same time the frames changes slower when holding the mouse over the clockwise throbber to symbolize how "fast" humans thinks, it changes 1 frame pr. second to relate directly to human time. I have created a lot of functions for this project to help structure the code and make it more manageable for myself as well as others. 
 

### **My project (conceptually)** ###

I wanted to explore the relation between how humans relate to/thinks about time and how a computer/machine thinks about time. My throbber consisting of lines imitates a clock to symbolise the human time, and how humans relate to time. A clock is the same all around the world even when the time isen't. Like Lammerant writes about in his text _“How humans and machines negotiate experience of time” _ (2018) humans time experiences is linked to our natural life cycles and rythms, like sundials which divides everyday into day and night, which decides when to be awake and when to sleep (p.89). Later on humans created the clock, which is what one part of my throbber is suposed to symbolize (p.90).The other throbber is created to look like a typical throbber know from youtube, netflix and many more digital sites. It is meant to symbolise the way computers relate to time as a much more abstract phenomena. Time can mean a lot a things when you look on computers and time, for an example computers have their own time cycle but synchronize their times when working together. The part of my throbber that symbolize the computer time consists of layers of three throbbers spining in different directions. This is meant to show the way computers often think and works in layers, consisting of software and hardware (p.92). But when you see a throbber on your screen you don't get to see all the layers that is working "underneath".

### **Mobilepay's throbber** ###

I have chosen to think about and look at the throbber that occurs in Mobilepay when you transfer someone money. The throbber is a line spining around in blue tones (see picture above). The thrbober in Mobilepay communicates that the it is sending the money, but what really happens behind the throbber? When sending someone money via Mobilepay, you send the money via their phone number, but what really happens is that you are transfering money to their account that are linked up with the phone number, from your account that is linked up with your phone number. But the throbber hides the whole transaction that happens in the bank and on the accounts. The whole idea with sending someone money via their phone number is connected with the conceptuel idea we have with sending a text message (sms). 

### **References** ###
References that I have looked at for inspiration:

- Daniel Shiffman, Code! Programming with p5.js, The Coding Train. Available at https://www.youtube.com/watch?v=1Osb_iGDdjk (3.1, 3.4, 5.1, 5.2)
- Soon, Winnie & Cox, Geoff. Aesthetic Programming: A Handbook of Software Studies. London: Open Humanities Press, 2020. p. 77-82. 
- Hans Lammerant, “How humans and machines negotiate experience of time,” in The Techno-Galactic Guide to Software Observation, 88-98, (2018) 
